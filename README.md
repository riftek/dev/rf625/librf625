# librf625
Riftek RF625 SDK
================

## Contents

- [Requirements](#requirements)
- [Building](#building)
- [Add the SDK to your project](#add-the-sdk-to-your-project)
- [License](#license)

## Requirements

* A compiler with C++11 support
* RF625 scanner should be upgraded to firmware version >= 20170516
You may download latest version 20190903 from [there](https://riftek.com/downloads/current/rf625fw_20190903.zip)

## Building

##### On Linux (g++)
```
make librf625.so
```
##### On Windows (MSVC)
```
Winmake.bat
```
Or open `msvc2017.sln` and build it in Visual Studio 2017

## Add the SDK to your project

##### Qt

Add `INCLUDEPATH += path_to_library_source` and `LIBS += -Lpath_to_module -l$$qtLibraryTarget(rf625)`

##### MSVC

Use Project Properties dialog to add a path to library source to C/C++, Additional Include Directories and path to rf625.lib file to Librarian, General, Additional Library Directories. Add `rf625.lib` Additional Dependencies on the same page.

Alternatively you may add the library source code directly to your project's sources.

You may find a project named Sample (includes tcp.cpp sample) within `msvc2017.sln` solution to check MSVC project settings.

## License

This software is distributed under LGPL license

