CC = g++
#CC = x86_64-w64-mingw32-g++
CXX_FLAGS = -O3 -std=c++11 -fPIC -pthread

DEPENDS = src/rf625.cpp \
	  src/rf625.h \
	  src/sockfun.cpp

librf625.so: ${DEPENDS}
	${CC} -shared ${CXX_FLAGS} -o $@ src/rf625.cpp src/sockfun.cpp src/crc.cpp


test: librf625.so src/test.cpp
	${CC} ${CXX_FLAGS} src/test.cpp -o $@ -L. -lrf625

samples: librf625.so samples/udp_stream.cpp samples/udp_rate.cpp samples/tcp.cpp
	${CC} ${CXX_FLAGS} samples/udp_stream.cpp -I ./src -L. -lrf625 -o udp_stream -lcurses
	${CC} ${CXX_FLAGS} samples/udp_rate.cpp -I ./src -L. -lrf625 -o udp_rate -lcurses
	${CC} ${CXX_FLAGS} samples/tcp.cpp -I ./src -L. -lrf625 -o tcp -lcurses

clean:
	rm *.so test udp_stream udp_rate tcp

all: librf625.so test samples

.PHONY : librf625.so test clean all samples
