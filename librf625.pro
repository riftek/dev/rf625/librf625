# https://gitlab.com/riftek/dev/rf625/librf625

QT       -= core gui

# uncomment to build test binary
#CONFIG += test

DEFINES += RF625_LIBRARY

TARGET = $$qtLibraryTarget(rf625)
TEMPLATE = lib
#CONFIG += staticlib

test {
    DEFINES += TEST
    TARGET = test
    TEMPLATE = app
    CONFIG   += console
}

DESTDIR = $${PWD}

win32 {
    LIBS += -lws2_32
}

HEADERS += \
    src/rf625.h

SOURCES += \
    src/crc.cpp \
    src/rf625.cpp \
    src/sockfun.cpp \
    src/wshlp.cpp

test: SOURCES += src/test.cpp
