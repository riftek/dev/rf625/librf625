#include <iostream>
#ifdef _MSC_VER
#define NOMINMAX
#endif
#include "rf625.h"
#ifdef _MSC_VER
#undef NOMINMAX
#endif

#ifdef __linux__
#include <arpa/inet.h>
#endif

static int read_number(int min, int max, const char* title)
{
    int n;
    do {
        std::cout << "Enter " << title << " (" << min << "-" << max << "): ";
        std::cin >> n;
        if (std::cin.fail()) {
            n = std::numeric_limits<int>::min();
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
    while (n < min || n > max);
    return n;
}

int main(int argc, char *argv[])
{
    /* Initialize librf625 */
    rf625::init();

    /* Execute search for scanners */
    std::cout << "Searching..." << std::endl;
    rf625_list dev_list = rf625::search(3000);
    if (dev_list.empty()) {
        std::cout << "Nothing found" << std::endl;
        rf625::cleanup();
        return 1;
    }
    std::cout << std::endl << "#\t| S/N\t| Address" << std::endl;

    /* List scanners info */
    int i;
    for(i=0; i<dev_list.size(); i++)
    {
        auto dev = dev_list.at(i);
        if (!dev) continue;
        in_addr addr;
        uint32_t sn = dev->get_info().serial_number;
        addr.s_addr = dev->get_info().ip_address;
        std::cout << i << "\t| "
                  << sn << "\t| "
                  << inet_ntoa(addr) << "\t| " << std::endl;
    }

    int n = 0;
    if (dev_list.size() > 1)
    {
        n = read_number(0, dev_list.size()-1, "scanner number");
    }

    /* Connect to scanner */
    std::shared_ptr<rf625> s = dev_list[n];
    if (s->connect())
    {
        rf625_parameters params;
        int exp, llev;
        /* read parameters */
        if (s->read_params(params))
        {
            exp = params.exposure_time;
            llev = params.laser_level;

            /* Get input for some new values */
            std::cout << std::endl << "Current exposure time: " << exp << std::endl;
            exp = read_number(0, 3824, "new exposure time");
            std::cout << std::endl << "Current laser level: " << llev << std::endl;
            llev = read_number(0, 255, "new laser level");

            /* Apply new values */
            params.exposure_time = exp;
            params.laser_level = llev;
            if (!s->write_params(params)) {
                std::cout << "Failed to write parameters" << std::endl;
            }
        }
        else {
            std::cout << "Failed to read user parameters" << std::endl;
        }

        /* close connection */
        s->disconnect();
    }
    else {
        std::cout << "Failed to connect" << std::endl;
    }

    /* Cleanup librf625 */
    rf625::cleanup();

    return 0;
}
